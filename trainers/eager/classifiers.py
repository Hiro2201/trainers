# coding: utf-8
"""
Classifiers for TensorFlow Eager

"""

from trainers.eager.functions import *
from trainers.module import Core
import time


class ImageClassifer(Core):
    def __init__(self, model, loss_function, device):
        super(ImageClassifer, self).__init__(model, loss_function, device)

    def fit(self, optimizer, train_dataloader, epochs):
        print("Start Training...")
        start_time = time.time()
        train(
            model=self.model,
            loss_functions=self._loss_function,
            optimizer=optimizer,
            dataset=train_dataloader,
            epochs=epochs,
            device=self._device
        )
        print("Training finished after {:.3f}s.".format(time.time() - start_time))
        return self

    def evaluate(self, test_dataloader):
        evaluate(
            model=self.model,
            dataset=test_dataloader,
            device=self._device
        )
        return self


if __name__ == '__main__':
    pass
