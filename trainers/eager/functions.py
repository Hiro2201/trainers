# coding: utf-8
"""
Functions for TensorFlow Eager

"""

import tensorflow as tf
import tensorflow.contrib.eager as tfe
import sys


def loss(model, x, y, loss_function, training=False, device="CPU:0"):
    with tf.device(device):
        prediction = model(x, training=training)
        return loss_function(y, prediction)


def grad(model, x, y, loss_functions, training=False, device="CPU:0"):
    with tf.device(device):
        with tf.GradientTape() as tape:
            loss_value = loss(model, x, y, loss_functions, training, device)
        return loss_value, tape.gradient(loss_value, model.variables)


def train(model, loss_functions, optimizer, dataset, epochs=1, device="CPU:0"):
    with tf.device(device):
        epoch_loss_avg = tfe.metrics.Mean()
        train_accuracy = tfe.metrics.Accuracy()

        for epoch in range(epochs):
            for (i, (x, y)) in enumerate(dataset):
                loss_value,  grads = grad(model, x, y, loss_functions=loss_functions, training=True, device=device)
                optimizer.apply_gradients(zip(grads, model.variables), global_step=tf.train.get_or_create_global_step())

                epoch_loss_avg(loss_value)
                train_accuracy(
                    tf.argmax(model(x), axis=1, output_type=tf.int32),
                    tf.argmax(y, axis=1, output_type=tf.int32)
                )

                if i % 100 == 0:
                    print("Loss: {:.3f} -- Accuracy: {:.3%}".format(epoch_loss_avg.result(), train_accuracy.result()))
            print("-"*50)
            print("Epochs: {:03d}/{:03d} | Loss: {:.3f} -- Accuracy: {:.3%}".format(
                epoch+1, epochs, epoch_loss_avg.result(), train_accuracy.result())
            )
            print("-"*50)


def evaluate(model, dataset, device="CPU:0"):
    with tf.device(device):
        test_loss_avg = tfe.metrics.Mean()
        test_accuracy = tfe.metrics.Accuracy()

        for (x, y) in dataset:
            test_loss_value = loss(model, x, y, device=device)
            test_loss_avg(test_loss_value)
            test_accuracy(
                tf.argmax(model(x), axis=1, output_type=tf.int32),
                tf.argmax(y, axis=1, output_type=tf.int32)
            )
        print("Loss: {:.3f} -- Accuracy: {:.3%}".format(test_loss_avg.result(), test_accuracy.result()))


if __name__ == '__main__':
    print("TensorFlow Version: {}".format(tf.__version__))
    print("TensorFlow will accelerate by GPU" if tf.test.is_gpu_available() else "TensorFlow runs on CPU")
