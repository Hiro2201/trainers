# coding: utf-8
"""
Classifiers for PyTorch

"""
from trainers.module import Core
from trainers.torch.functions import *
import torch
import time


class ImageClassifierWithLog:
    def __init__(self, model, loss_function, device, writer):
        self.model = model
        self._loss_function = loss_function
        self._device = device
        self._writer = writer

    def fit(self, optimizer, train_dataloader, epochs):
        print("Start Training...")
        start_time = time.time()
        train(
            model=self.model,
            loss_fnc=self._loss_function,
            dataloader=train_dataloader,
            optimizer=optimizer,
            device=self._device,
            epochs=epochs,
            writer=self._writer
        )
        print("Training finished after {:.3f}s.".format(time.time() - start_time))
        return self

    def evaluate(self, test_dataloader, projector):
        evaluate(
            model=self.model,
            loss_fnc=self._loss_function,
            dataloader=test_dataloader,
            device=self._device,
            writer=self._writer,
            projector=projector
        )
        return self

    def predict(self, input_data, verbose=False, classes=None):
        return predict(
            model=self.model,
            inputs=input_data,
            verbose=verbose,
            classes=classes
        )

    def export_logs(self, path):
        export_logs(writer=self._writer, path=path)

    def save_weight(self, path):
        torch.save(self.model.state_dict(), path)
        print("Done.")

    def load_weight(self, path):
        self.model.load_state_dict(torch.load(path))
        return self


class ImageClassifier(Core):
    def __init__(self, model, loss_function, device):
        super(ImageClassifier, self).__init__(model, loss_function, device)

    def fit(self, optimizer, train_dataloader, epochs):
        print("Start Training...")
        start_time = time.time()
        self.model.train()
        self.model = self.model.to(self._device)
        num_data = len(train_dataloader.dataset)

        for epoch in range(epochs):
            start_epoch = time.time()
            running_loss = 0.0
            running_correct = 0

            for step, (data, labels) in enumerate(train_dataloader):
                sys.stdout.write(
                    "\rEpochs: {:03d}/{:03d} - {:03d}% ".format(
                        epoch+1, epochs, int((train_dataloader.batch_size * (step + 1) / num_data)*100)
                    )
                )
                sys.stdout.flush()

                data = data.to(self._device)
                labels = labels.to(self._device).long()

                optimizer.zero_grad()
                outputs = self.model(data)

                if type(outputs) == tuple:
                    outputs, _ = outputs

                _, preds = torch.max(outputs, 1)
                loss = self._loss_function(outputs, labels)
                loss.backward()
                optimizer.step()

                running_loss += loss.item() * data.size(0)
                running_correct += torch.sum(preds == labels.data).item()

            epoch_loss = running_loss / num_data
            epoch_accuracy = running_correct / num_data

            print("| Loss: {:.3F} - Accuracy: {:.3%} Time: {:.3f}s ".format(
                epoch_loss, epoch_accuracy, time.time() - start_epoch)
            )

        print("Training finished after {:.3f}s".format(time.time() - start_time))
        return self

    def evaluate(self, test_dataloader):
        self.model.eval()
        self.model = self.model.to(self._device)
        running_loss = 0.0
        running_corrects = 0

        num_data = len(test_dataloader.dataset)

        with torch.no_grad():
            for data, labels in test_dataloader:
                data = data.to(self._device)
                labels = labels.to(self._device).long()

                outputs = self.model(data)

                if type(outputs) == tuple:
                    outputs, _ = outputs

                _, preds = torch.max(outputs, 1)
                loss = self._loss_function(outputs, labels)
                running_loss += loss.item() * data.size(0)
                running_corrects += torch.sum(preds == labels.data).item()

        test_loss = running_loss / num_data
        test_accuracy = running_corrects / num_data
        print("Loss: {:.3f} - Accuracy: {:.3%}".format(test_loss, test_accuracy))
        return self

    def predict(self, input_data, verbose=False, classes=None):
        return predict(
            model=self.model,
            inputs=input_data,
            verbose=verbose,
            classes=classes
        )

    def load_weight(self, path, device=None):
        load_weight(self.model, path=path, device=device)
        return self

    def save_weight(self, path):
        save_weight(self.model, path=path)
        print("Done.")
