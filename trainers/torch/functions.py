# coding: utf-8
"""
Functions for PyTorch

"""

import sys
import torch
from torch.nn import functional as F
import time
import numpy as np
from PIL import Image
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from torchvision import transforms


def train(model, loss_fnc, optimizer, dataloader, device, writer, epochs=1):
    model.train()
    model = model.to(device)
    num_data = len(dataloader.dataset)
    global_step = 0

    for epoch in range(epochs):
        start_epoch_time = time.time()
        running_loss = 0.0
        running_correct = 0

        for step, (data, labels) in enumerate(dataloader):
            sys.stdout.write(
                "\rEpochs: {:03d}/{:03d} - {:03d}% ".format(
                    epoch+1, epochs, int((dataloader.batch_size * (step + 1) / num_data)*100)
                )
            )
            sys.stdout.flush()

            global_step += 1
            data = data.to(device)
            labels = labels.to(data).long()

            optimizer.zero_grad()
            outputs = model(data)

            if type(outputs) == tuple:
                outputs, _ = outputs

            _, predictions = torch.max(outputs, 1)
            loss = loss_fnc(outputs, labels)

            loss.backward()
            optimizer.step()

            running_loss += loss.item() * data.size(0)
            running_correct += torch.sum(predictions == labels.data).item()

            if step % 100 == 0:
                writer.add_scalar("train/train_loss", loss.item(), global_step)  # loss.item() -> float

            if epoch == 0 and step == 0:
                writer.add_graph(model, data)

            for name, params in model.named_parameters():
                writer.add_histogram(name, params.clone().cpu().data.numpy(), global_step)

        epoch_loss = running_loss / num_data
        epoch_accuracy = running_correct / num_data

        print("| Loss: {:.3f} - Accuracy: {:.3%} Time: {:.3f}s ".format(
            epoch_loss, epoch_accuracy, time.time() - start_epoch_time)
        )
        writer.add_scalar("train/train_accuracy", epoch_accuracy * 100, global_step)


def evaluate(model, loss_fnc, dataloader, writer, device, projector=False):
    global label_imgs, features, all_labels
    model.eval()
    model = model.to(device)
    running_loss = 0.0
    running_corrects = 0
    global_step = 0

    if projector:
        # features = torch.zeros(0)
        # all_labels = torch.zeros(0)
        # label_imgs = torch.zeros(0)
        features = np.zeros(0)
        all_labels = np.zeros(0)
        label_imgs = np.zeros(0)

    num_data = len(dataloader.dataset)

    with torch.no_grad():
        for data, labels in dataloader:
            data = data.to(device)
            labels = labels.to(device).long()
            global_step += 1

            outputs = model(data)

            if type(outputs) == tuple:
                outputs, _ = outputs

            _, predictions = torch.max(outputs, 1)
            loss = loss_fnc(outputs, labels)

            running_loss += loss.item() * data.size(0)
            running_corrects += torch.sum(predictions == labels.data)

            writer.add_scalar("test/test_loss", loss.item(), global_step)

            if projector:
                if device != "cpu":
                    # features = torch.cat((features.cpu(), outputs.cpu()))
                    # label_imgs = torch.cat((label_imgs.cpu(), data.cpu()))
                    features = np.concatenate((features.cpu().numpy(), outputs.cpu().numpy()))
                    label_imgs = np.concatenate((label_imgs.cpu().numpy(), outputs.cpu().numpy()))

                else:
                    # features = torch.cat((features, outputs))
                    # label_imgs = torch.cat((label_imgs, data))
                    features = np.concatenate((features.numpy(), outputs.numpy()))
                    label_imgs = np.concatenate((label_imgs.numpy(), outputs.numpy()))

                # all_labels = torch.cat((all_labels, labels.cpu().float()))
                all_labels = np.concatenate((all_labels, labels.cpu().numpy()))

    test_loss = running_loss / num_data
    test_acc = running_corrects.float() / num_data

    print("Loss: {:.3f} - Accuracy: {:.3%}".format(test_loss, test_acc))

    if projector:
        writer.add_embedding(features, metadata=all_labels.numpy(), label_img=label_imgs, global_step=global_step)


def predict(model, inputs, verbose=False, classes=None):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    model.eval()
    model = model.to(device)

    if type(inputs) is not torch.Tensor:
        inputs = torch.Tensor(inputs)

    inputs = inputs.unsqueeze(0)
    inputs = inputs.to(device)

    with torch.no_grad():
        output = model(inputs)

        if type(output) == tuple:
            output, _ = output

        _, prediction = torch.max(output, 1)

    if verbose and type(classes) is list:
        result = "Prediction: {} ({:.3%})".format(
            classes[prediction.cpu().numpy()[0]], F.softmax(output, dim=1).cpu().numpy().max())

    elif verbose and classes == "roc":
        result = prediction.cpu().numpy(), F.softmax(output, dim=1).cpu().numpy()

    else:
        result = prediction.cpu().numpy()[0], F.softmax(output, dim=1).cpu().numpy().max()

    return result


def export_logs(writer, path):
    writer.export_scalars_to_json(path)
    writer.close()


def save_weight(model, path):
    torch.save(model.state_dict(), path)


def load_weight(model, path, device=None):
    if device is None and torch.cuda.is_available():
        model.load_state_dict(torch.load(path))
    elif device is not None:
        model.load_state_dict(torch.load(path, map_location=device))
    else:
        model.load_state_dict(torch.load(path, map_location="cpu"))


class Img2Tensor:
    def __init__(self, imagenet=True):
        """
        :param imagenet: もしTrueならPyTorchのImageNetで学習済みモデルが期待する正規化を適用する。
        """
        if imagenet:
            self.__mean = np.array([0.485, 0.456, 0.406])
            self.__std = np.array([0.229, 0.224, 0.225])
        else:
            self.__mean = None
            self.__std = None

    def set_normalize(self, mean, std):
        """
        イニシャライザでFalseを洗濯した時に実行。
        :param mean: Numpy Array
        :param std: Numpy Array
        :return:
        """
        self.__mean = mean
        self.__std = std
        return self

    def to_tensor(self, path, size=299):
        """

        :param path: file path (recommend full-path)
        :param size: for resizing image.
        :return: torch.Tensor (c, h, w)
        """
        img = Image.open(path)
        img = transforms.Resize(size).__call__(img)
        img = transforms.CenterCrop(size).__call__(img)
        img = transforms.ToTensor().__call__(img)
        img = transforms.Normalize(mean=self.__mean, std=self.__std).__call__(img)
        return img


"""

事実\予測    | `たこ焼き`と予測 | `たこ焼き` 以外と予測 | 
------------+----------------+--------------------+
`たこ焼き`   |    30          | 15                 |
------------+----------------+--------------------|
`たこ焼き以外`|    10          |  25                |
---------------------------------------------------


たこ焼き 精度 : 適合率 30 / 40 = 0.75, 再現率 30 / 45 = 0.667

* 適合率: 嘘つき度, 再現率: 網羅度

ROC 曲線

"""


class Analytics:
    def __init__(self, classifier, num_classes):
        self.classifier = classifier
        self.table = np.zeros((num_classes, num_classes), dtype=np.uint32)
        self.num_classes = num_classes
        self.relevance = []
        self.recall = []

        self._did_evaluate = False
        self._is_calc_relevance = False
        self._is_calc_recall = False
        self.__device = "cuda:0" if torch.cuda.is_available() else "cpu"

    def evaluate(self, test_dataloader):
        """
        Required batch_size=1
        :param test_dataloader: DataLoader(TensorDataset, batch_size=1)
        :return:
        """
        self.classifier.model.eval()
        with torch.no_grad():
            for data, label in test_dataloader:
                data = data.to(self.__device)
                label = label.to(self.__device)
                output = self.classifier.model(data)

                if type(output) is tuple:
                    output, _ = output

                _, pred = torch.max(output, 1)
                self.table[label.cpu().numpy()[0]][pred.cpu().numpy()[0]] += 1

        self._did_evaluate = True
        return self

    def calculate_relevance(self):
        """
        適合率の計算
        :return:
        """
        for i in range(self.num_classes):
            self.relevance.append(self.table[i][:] / np.sum(self.table, axis=0))

        self._is_calc_relevance = True
        return self

    def calculate_recall(self):
        """
        再現度の計算
        :return:
        """
        for i in range(self.num_classes):
            self.recall.append(self.table[:][i] / np.sum(self.table, axis=1))

        self._is_calc_recall = True
        return self

    def plot(self, class_name=None, **kwargs):
        is_table = kwargs.get("table", True)
        is_relevance = kwargs.get("relevance", True)
        is_recall = kwargs.get("recall", True)

        if is_table and self._did_evaluate:
            plt.figure(figsize=(10, 8))
            table = pd.DataFrame(self.table)
            if type(class_name) is list:
                table.index = class_name
                table.columns = ["predict" + str(i) for i in class_name]

            plt.title("Table")

            sns.heatmap(table, cmap="hot", annot=True, fmt="d")
            plt.show()

        if is_relevance and self._is_calc_relevance:
            plt.figure(figsize=(10, 8))
            self.relevance = np.array(self.relevance)
            relevance = pd.DataFrame(self.relevance)
            if type(class_name) is list:
                relevance.index = class_name
                relevance.columns = ["predict" + str(i) for i in class_name]
            plt.title("Relevance Rate Table")

            sns.heatmap(relevance, cmap="hot", annot=True, fmt="1.3f")
            plt.show()

        if is_recall and self._is_calc_recall:
            plt.figure(figsize=(10, 8))
            self.recall = np.array(self.recall)
            recall = pd.DataFrame(self.recall)
            if type(class_name) is list:
                recall.index = class_name
                recall.columns = ["predict" + str(i) for i in class_name]
            plt.title("Recall Table")

            sns.heatmap(recall, cmap="hot", annot=True, fmt="1.3f")
            plt.show()

    def export_csv(self, table_name, relevance_name, recall_name):
        pd.DataFrame(self.table).to_csv(table_name)
        pd.DataFrame(self.relevance).to_csv(relevance_name)
        pd.DataFrame(self.recall).to_csv(recall_name)


if __name__ == '__main__':
    print("PyTorch Version {}".format(torch.__version__))
    print("Can Use CUDA" if torch.cuda.is_available() else "PyTorch runs on CPU")
