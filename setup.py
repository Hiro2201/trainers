# coding: utf-8
from setuptools import setup, find_packages

requires = [
            "tensorboardX >= 1.4"
            ]

setup(
    name="trainers",
    version='0.0.2 alpha',
    description="Trainer for PyTorch",
    url="https://gitlab.com/Hiro2201/trainers",
    author="Hirotaka Kawashima",
    author_email="kawashima34@gmail.com",
    license='MIT',
    packages=find_packages(),
    install_requires=requires,
)
