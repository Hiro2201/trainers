# Trainers

```python
import torch
from torch import nn
from torch import optim
from torch.nn import functional as F
from torch.utils import data
from tensorflow import keras
from trainers.torch.classifiers import ImageClassifier

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

x_train = x_train.reshape(-1, 1, 28, 28)
x_test = x_test.reshape(-1, 1, 28, 28)

x_train = torch.Tensor(x_train)
y_train = torch.LongTensor(y_train)

x_test = torch.Tensor(x_test)
y_test = torch.LongTensor(y_test)

train_dataset = data.TensorDataset(x_train, y_train)
test_dataset = data.TensorDataset(x_test, y_test)

train_loader = data.DataLoader(train_dataset, batch_size=512, shuffle=True)
test_loader = data.DataLoader(test_dataset, batch_size=512, shuffle=False)


class CNNModel(nn.Module):
    def __init__(self):
        super(CNNModel, self).__init__()

        self.conv1 = nn.Conv2d(1, 32, 3, padding=1)
        self.conv2 = nn.Conv2d(32, 64, 3, padding=1)

        self.dropout1 = nn.Dropout2d(0.25)
        self.dropout2 = nn.Dropout2d(0.5)

        self.fc1 = nn.Linear(64 * 14 * 14, 128)
        self.fc2 = nn.Linear(128, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, (2, 2))
        x = self.dropout1(x)

        x = x.view(-1, 64 * 14 * 14)

        x = F.relu(self.fc1(x))
        x = self.dropout2(x)
        return self.fc2(x)


model = CNNModel()
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
loss_func = nn.CrossEntropyLoss()
device = "cuda:0" if torch.cuda.is_available() else "cpu"
print(device)


classifier = ImageClassifier(model, loss_func, device)
classifier.fit(optimizer, train_loader, epochs=5).evaluate(test_loader)

```
